var gulp = require('gulp');
var sass = require('gulp-sass');
var pug = require('gulp-pug');


gulp.task('sass',function(){
    return gulp.src('./src/sass/**/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('./build/css'))
});

gulp.task('pug',function () {
    return gulp.src('./src/views/index.pug')
        .pipe(pug({pretty:true}))
        .pipe(gulp.dest('./build/'))
});


gulp.task('watch',['pug','sass'],function () {
    gulp.watch('./src/sass/**/*.scss',['sass']);
    gulp.watch('./src/views/**/*.pug',['pug']);
});